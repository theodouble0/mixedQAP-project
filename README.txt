Authors: 
  Sébastien Verel

Date: 
  2020/12/30 - Version 1


*** Description:

Please see the project description at: 
https://www.overleaf.com/read/phjcbfrmfycr


*** Instance files:

The directory instances/ contains the set of mixed-variable Quadratic Assignment Problem (mixed-QAP) instance files.


*** Code:

The directory code/src/ the c++ code. The evaluation function is in the directory code/src/evaluation, and basic tests are in the directory src/tests.


*** To compile the code:

cd code
mkdir build
cmake ../src/tests
make


*** To execute the tests:

./t-solution
./t-eval

